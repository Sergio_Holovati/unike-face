import { Injectable } from "@angular/core";
import {AppUI, AsThreshold, Template} from '@identy/identy-face';
import {Subject} from "rxjs";
import {MenuSelection} from "../../../modules/models/menu-selection";

@Injectable({
  providedIn: "root"
})
export class ContextMenuHelperService {

  public selection: MenuSelection;

  public readonly enableAsSelection: Subject<boolean>;
  public readonly showCaptureTrainingSelection: Subject<boolean>;
  public readonly templateSelection: Subject<Template[]>;
  public readonly uISelection: Subject<AppUI>;
  public download: Subject<boolean>;
  public assisted: Subject<boolean>;
  public asThresholdSelection: Subject<AsThreshold>;


  constructor() {

    this.enableAsSelection = new Subject();
    this.showCaptureTrainingSelection = new Subject();
    this.templateSelection = new Subject();
    this.uISelection = new Subject();
    this.selection = new MenuSelection();
    this.download = new Subject<boolean>();
    this.assisted = new Subject<boolean>();
    this.asThresholdSelection = new Subject<AsThreshold>();

    this.bind();

  }

  bind() {
    this.enableAsSelection.subscribe((selected) => {
      this.selection.enableAS = selected;
    });

    this.showCaptureTrainingSelection.subscribe((selected) => {
      this.selection.showCaptureTraining = selected;
    });

    this.templateSelection.subscribe((templates) => {
      this.selection.templates = templates;
    });

    this.uISelection.subscribe((appui) => {
      this.selection.uiSelect = appui;
    });

    this.download.subscribe((down) => {
      this.selection.download = down;
    });

    this.assisted.subscribe((selected) => {
      this.selection.assisted = selected;
    });

    this.asThresholdSelection.subscribe((thresold) => {
      this.selection.asThresold = thresold;
    });
  }

}
