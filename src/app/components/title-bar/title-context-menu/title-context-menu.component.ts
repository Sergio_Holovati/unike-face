import {Component, HostBinding, HostListener, OnInit} from '@angular/core';
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {ContextMenuHelperService} from "./context-menu-helper.service";
import {AppUI, AsThreshold, Template} from '@identy/identy-face';
import {AppUiOption, AsThreshOption, TemplateOption} from '../../../modules/models/types';
import {Router} from "@angular/router";
import $ from "jquery";

@Component({
  selector: "app-title-context-menu",
  templateUrl: "./title-context-menu.component.html",
  styleUrls: ["./title-context-menu.component.css"]
})
export class TitleContextMenuComponent implements OnInit {

  dropdown: any;

  templates: Array<TemplateOption>;
  selectedAppUi: AppUiOption;
  appuis: Array<AppUiOption>;
  isOpen: boolean;
  asThresold: Array<AsThreshOption>;

  enableAs: boolean;
  captureTraining: boolean;
  download: boolean;
  assisted: boolean;
  asThresholdSelection: AsThreshold = AsThreshold.LOW;

  @HostListener( "document:click", ["$event"])  docClick(event) {
    if (event.target.classList.contains("dropdown-toggle")) {
      return;
    }
    if (this.isOpen && $(event.target).closest("ul").length === 0) {
      $(".btn-menu").trigger("click");
    }
  }
  constructor(private modalService: BsModalService, private contextMenuHelper: ContextMenuHelperService,  private router: Router) {

    this.enableAs = true;
    this.captureTraining = true;
    this.download = false;
    this.assisted = false;
    if (localStorage.getItem("downloadMode")) {
      this.download = localStorage.getItem("downloadMode") === "true";
    }
    this.templates = [
      {
        name: "ISO_19794_5",
        selected: false,
        template: Template.ISO_19794_5
      },
      {
        name: "PNG",
        selected: true,
        template: Template.PNG
      },
      {
        name: "JPEG",
        selected: true,
        template: Template.JPEG
      }
    ];

    this.appuis = [
      {
        name: "SELF SERVICE",
        selected: false,
        appui: AppUI.BOXES
      }
    ];

    this.asThresold = [
      {
        name: "VERY_HIGH",
        selected: false,
        threshold: AsThreshold.VERY_HIGH
      },
      {
        name: "HIGH",
        selected: false,
        threshold: AsThreshold.HIGH
      },
      {
        name: "MEDIUM",
        selected: false,
        threshold: AsThreshold.MEDIUM
      },
      {
        name: "LOW",
        selected: true,
        threshold: AsThreshold.LOW
      }
    ];

    this.dropdown = {
      uiSelection: false,
      cmpSelection: false,
      tmpSelection: false,
      thrSelection: false
    };

    this.selectedAppUi = this.appuis[0];

    this.update();
  }

  ngOnInit() {

  }

  update() {

    setTimeout(() => {

      this.contextMenuHelper.enableAsSelection.next(this.enableAs);
      this.contextMenuHelper.download.next(this.download);

      this.contextMenuHelper.templateSelection.next(this.templates.filter((template) => {
        return template.selected;
      }).map((template) => {
        return template.template;
      }));

      this.contextMenuHelper.uISelection.next(this.selectedAppUi.appui);
      this.contextMenuHelper.showCaptureTrainingSelection.next(this.captureTraining);
      this.contextMenuHelper.assisted.next(this.assisted);
      this.contextMenuHelper.asThresholdSelection.next(this.asThresholdSelection);

    }, 10);

  }

  doOpen(acc: String) {

    for (const key of Object.keys(this.dropdown)) {
      if (key !== acc ) {
          this.dropdown[key] = false;
      }
    }

  }

  onShown() {
    this.isOpen = true;
  }
  onHide() {
    this.isOpen = false;
  }

}
