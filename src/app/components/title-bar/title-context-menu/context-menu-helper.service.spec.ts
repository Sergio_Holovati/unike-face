import { TestBed } from '@angular/core/testing';

import { ContextMenuHelperService } from './context-menu-helper.service';

describe('ContextMenuHelperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContextMenuHelperService = TestBed.get(ContextMenuHelperService);
    expect(service).toBeTruthy();
  });
});
