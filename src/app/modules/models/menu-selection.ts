import {AppUI, AsThreshold, Template} from '@identy/identy-face';

export class MenuSelection {

  public enableAS: boolean;
  public showCaptureTraining: boolean;
  public download: boolean;

  public templates: Template[];
  public uiSelect: AppUI;
  public assisted: boolean;
  public asThresold: AsThreshold;

  constructor() {}


}
