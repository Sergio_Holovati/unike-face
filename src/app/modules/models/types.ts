import {AppUI, AsThreshold, Template} from '@identy/identy-face';

export interface TemplateOption {
  name: string;
  selected: boolean;
  template: Template;
}

export interface AppUiOption {
  name: string;
  selected: boolean;
  appui: AppUI;
}

export interface AsThreshOption {
  name: string;
  selected: boolean;
  threshold: AsThreshold;
}
