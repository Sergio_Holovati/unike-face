import {Component, OnInit} from "@angular/core";
import {ajax} from "jquery";
import {Base64, FaceSDK, LocalizationMessage, SdkOptionsType, TransactionMode} from "@identy/identy-face";
import {ContextMenuHelperService} from "../../../components/title-bar/title-context-menu/context-menu-helper.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {ProgressDialogComponent} from "../../../components/dialogs/progress-dialog/progress-dialog.component";
import {TransactionDialogComponent} from "../../../components/dialogs/transaction-dialog/transaction-dialog.component";
import {ZipModule} from "src/app/modules/zip-module";
import {saveAs} from "file-saver";
import {ActivatedRoute, Router} from "@angular/router";
import * as LogRocket from "logrocket";
import {Utils} from "../../../modules/helpers/Utils";
import {environment} from "../../../../environments/environment";

@Component({
  selector: "app-sdk",
  templateUrl: "./sdk.component.html",
  styleUrls: ["./sdk.component.css"]
})
export class SdkComponent implements OnInit {

  bsModelPrgRef2: BsModalRef;
  bsModelTxnDialog: BsModalRef;
  action: string;
  redirect: string;

  constructor(private contextMenuSelection: ContextMenuHelperService,
              private modalService: BsModalService, private route: ActivatedRoute ) {
    let device = null;
    try {
      fetch("https://wurfl.io/wurfl.js").then(async (r) => {
        const str = await r.text();
        (0, eval)(str);
        // @ts-ignore
        if (WURFL !== void 0) {
          // @ts-ignore
          device = WURFL;

          LogRocket.init('tgsvwu/face_debug-hy2np');
          LogRocket.identify({
            userID: (localStorage.getItem("lu") !== null ? localStorage.getItem("lu") : "default") + " : " + device.complete_device_name,
            group: Utils.uuidv4()
          });
          this.getUser();
        }
      });
    } catch (e) {
      console.log(e);
    }
  }


  ngOnInit() {
    this.action = this.route.snapshot.queryParamMap.get("action");
    this.redirect = this.route.snapshot.queryParamMap.get("redirect");
    setTimeout(()=> {
      this.runTransaction(TransactionMode.CAPTURE);
    }, 100);
    if (this.action === "CAPTURE") {
        
    } else if (this.action === "ENROLL") {
        setTimeout(()=> {
          this.runTransaction(TransactionMode.ENROLL);
        }, 100);
    } else if (this.action === "VERIFY") {
        setTimeout(()=> {
          this.runTransaction(TransactionMode.VERIFY);
        }, 100);
    }
  }

  getUser() {
    let user = localStorage.getItem("lu");
    if (user === null) {
      user = prompt("Seu nome por gentileza");
      if (user === null || user === "") {
        alert("A name is required");
        this.getUser();
      } else {
        localStorage.setItem("lu", user);
      }
    }
  }

  runTransaction(transaction: TransactionMode) {
    const selection = this.contextMenuSelection.selection;
    const options: SdkOptionsType = {
      enableAS: selection.enableAS,
      requiredTemplates: selection.templates,
      showCaptureTraining: false, // selection.showCaptureTraining,
      base64EncodingFlag: Base64.NO_WRAP,
      allowClose: true,
      transaction: {
        type: transaction
      },
      debug: false,
      asThreshold: selection.asThresold,
      assisted: selection.assisted,
      urls: {
        modelURL: `${environment.url}/api/v1/model`
      },
      backend: "webgl",
      graphics: {
        canvas: {
          label: "white",
          canvasBackground: "rgba(206, 150, 243, 0)",
          labelBackground: "#3d5272",
          ovalBorderBackground: "#61b15a"
        }
      },
      localization: {
        en: {
          FEEDBACK_RETRY: "Perhaps the location where you are trying to capture is not suitable or you tried to capture from a video / photo",
          FEEDBACK_RETRY_QUALITY: "Bad Quality, Retry Capture",
          FEEDBACK_ENROLLED: "Registered Successfully",
          FEEDBACK_CAPTURED: "Captured Successfully",
          FEEDBACK_NOT_ENROLLED: "User Not Enrolted",
          FEEDBACK_VERIFY_COMPLETED: "Verification Successful",
          FEEDBACK_VERIFY_FAILED: "Verification failed",
          FEEDBACK_SEARCHING: "Searching...",
          FEEDBACK_INSIDE_GUIDE: "Please be inside the guide",
          FEEDBACK_PLEASE_HOLD: "Please hold.",
          FEEDBACK_STABLE: "Please be stable",
          FEEDBACK_NOT_STABLE: "You are moving a lot",
          FEEDBACK_EYE_LEVEL: "Look Straight",
          FEEDBACK_FACE_OUTSIDE: "Please be inside guide",
          FEEDBACK_CLOSE: "Move Away",
          FEEDBACK_FAR: "Move Close",
          FEEDBACK_LEFT_FACING: "Look Straight",
          FEEDBACK_RIGHT_FACING: "Look Straight",
          FEEDBACK_ROTATED: "Look Straight",
          FEEDBACK_OK: "Please hold",
          FEEDBACK_NO: "Checking",
          FEEDBACK_NOT_CENTERED: "Center Face in oval",
          FEEDBACK_CAMERA_ACQUIRING_FAILED: "Cannot acquire camera, Allow permission Or Retry Capture",
          FEEDBACK_PROCESSING: "Processing...",
          FEEDBACK_UP_FACING: "Lower your face a little",
          FEEDBACK_UPLOAD_FAILURE: "Internal error retry",
          FEEDBACK_INITIALIZATION: "Initializing...",
          FEEDBACK_CHANGE_LIGHT: "Avoid shadows",
          FEEDBACK_BUTTON_CLOSE: "Close",
          FEEDBACK_BUTTON_RETRY: "Retry",
          FEEDBACK_TRAINING_LABEL: "Dont Show again",
          FEEDBACK_ORIENTATION_NOT_SUPPORTED: "Only Portrait mode is supported",
          ERROR_BROWSER_NOT_SUPPORTED: "Browser not supported, Please update to Latest Chrome, Firefox (Android) or Safari on IOS.",
          ERROR_WEBRTC_NOT_SUPPORTED: "Webrtc not supported",
          ERROR_MODEL_FAIL: "Model detection failed",
          ERROR_SERVER_INTERNAL: "Internal Server Error",
          ERROR_SERVER_CONNECTION_FAILURE: "Server connection failure"         
        },
        'pt-br': {
          FEEDBACK_RETRY: "Talvez esse local onde você está tentando fazer a captura não seja adequado ou você tentou capturar de um video / foto",
          FEEDBACK_RETRY_QUALITY: "Qualidade de captura insuficiente, repetir captura",
          FEEDBACK_ENROLLED: "Cadastro realizado com sucesso",
          FEEDBACK_CAPTURED: "Captura efetuada com sucesso",
          FEEDBACK_NOT_ENROLLED: "Usuário não cadastrado",
          FEEDBACK_VERIFY_COMPLETED: "Usuário validado com sucesso",
          FEEDBACK_VERIFY_FAILED: "Usuário não validado",
          FEEDBACK_SEARCHING: "Pesquisando...",
          FEEDBACK_INSIDE_GUIDE: "Mantanha o rosto na moldura",
          FEEDBACK_PLEASE_HOLD: "Aguarde...",
          FEEDBACK_STABLE: "Por favor, mantanha essa posição",
          FEEDBACK_NOT_STABLE: "Você está se mexendo muito",
          FEEDBACK_EYE_LEVEL: "Olhe para frente",
          FEEDBACK_FACE_OUTSIDE: "Encaixe a face na muoldura",
          FEEDBACK_CLOSE: "Afaste-se um pouco",
          FEEDBACK_FAR: "Aproxime um pouco mais",
          FEEDBACK_LEFT_FACING: "Mantenha a face no centro da moldura",
          FEEDBACK_RIGHT_FACING: "Mantenha a face no centro da moldura",
          FEEDBACK_ROTATED: "Mantenha a face no centro da moldura",
          FEEDBACK_OK: "Mantenha nessa posição",
          FEEDBACK_NO: "Verificando...",
          FEEDBACK_NOT_CENTERED: "Face fora da moldura",
          FEEDBACK_CAMERA_ACQUIRING_FAILED: "Não é possível iniciar a câmera, conceda permissão ou tente capturar novamente",
          FEEDBACK_PROCESSING: "Processando ...",
          FEEDBACK_UP_FACING: "Abaixe um pouco sua face",
          FEEDBACK_UPLOAD_FAILURE: "Opsss... Ocorreu um erro estranho, tente novamente",
          FEEDBACK_INITIALIZATION: "Inicializando ...",
          FEEDBACK_CHANGE_LIGHT: "Evite locais com sombras",
          FEEDBACK_BUTTON_CLOSE: "Fechar",
          FEEDBACK_BUTTON_RETRY: "Tentar novamente",
          FEEDBACK_TRAINING_LABEL: "Não mostrar novamente",
          FEEDBACK_ORIENTATION_NOT_SUPPORTED: "Apenas o modo Retrato é compatível",
          ERROR_BROWSER_NOT_SUPPORTED: "Navegador não compatível, atualize para o Chrome mais recente, Firefox (Android) ou Safari no IOS.",
          ERROR_WEBRTC_NOT_SUPPORTED: "Webrtc não suportado",
          ERROR_MODEL_FAIL: "Falha na detecção do modelo. Entre em contato com suporte.unike.tech",
          ERROR_SERVER_INTERNAL: "Erro interno do servidor. Entre em contato com suporte.unike.tech",
          ERROR_SERVER_CONNECTION_FAILURE: "Falha na conexão do servidor. Entre em contato com suporte.unike.tech"
        }        
      }
    };

    if (transaction === TransactionMode.VERIFY) {
      if (!options.transaction) {
        options.transaction = {};
      }
      options.transaction.id = localStorage.getItem("transactionId");
    }

    const faceSDK = new FaceSDK(options);

    faceSDK.onInit = () => {
      faceSDK.capture().then((blob: Blob) => {
        return this.postData(faceSDK, blob, transaction);
      }).catch((err) => {

        if (err instanceof LocalizationMessage) {

          if (err.code !== 202) {
            alert(err.getLocalizedString());
          }
        }

      });
    };

    faceSDK.initialize().catch((err) => {

      if (err instanceof LocalizationMessage) {
        if (err.code === 100  || err.code === 500) {
          alert(err.getLocalizedString());
        }
      }

    });
  }

  postData(sdk: FaceSDK, capresult: Blob, action: TransactionMode) {

    return new Promise<void>((resolve => {

      const fd = new FormData();
      fd.append("file", capresult, `bdata`);

      this.bsModelPrgRef2 = this.modalService.show(ProgressDialogComponent, Object.assign({}, {
        class: "modal-dialog-centered modal-sm modal-dialog-progress",
        backdrop: false,
        ignoreBackdropClick: true
      }));

      ajax({
        url: `${environment.url}/api/v1/process?ts=${new Date().getTime()}`,
        contentType: false,
        processData: false,
        method: "POST",
        dataType: "JSON",
        data: fd,
        headers : {
          "X-DEBUG": localStorage.getItem("lu")
        }
      }).done((response: any) => {

        console.info(response.data)

        setTimeout(() => {
          if (this.bsModelPrgRef2) {
            this.bsModelPrgRef2.hide();
            this.bsModelPrgRef2 = null;
          }
        }, 10);

        if (this.action && this.redirect) {
          window.location.assign(this.redirect);
          return;
        }

        const clientResponse = {transaction_id: response.id, code: response.code, feedback_code: response.message};
        if (action === TransactionMode.ENROLL) {
          localStorage.setItem("transactionId", clientResponse.transaction_id.toString());
        }

        this.bsModelTxnDialog = this.modalService.show(TransactionDialogComponent, {
          class: "modal-md txn-modal-dialog ",
          initialState: {
            data: clientResponse,
            options: sdk.getOptions(),
            localization: sdk.localization,
            aligned: "data:image/jpg;base64," + response.data.templates.JPEG,
            onclose: () => {
              if (sdk.getOptions().transaction.type === TransactionMode.ENROLL) {
                localStorage.setItem("transactionId", clientResponse.transaction_id.toString());
              }

              if (this.contextMenuSelection.selection.download) {
                new ZipModule().package(response, false).then((zipv) => {
                  saveAs(zipv, `face-templates-${new Date().getTime()}.zip`);
                });
              }

              this.bsModelTxnDialog.hide();
              resolve();
            }
          }
        });

      }).fail((err) => {

        setTimeout(() => {
          if (this.bsModelPrgRef2) {
            this.bsModelPrgRef2.hide();
            this.bsModelPrgRef2 = null;
          }
        }, 10);

        const message = err.responseJSON.message;
        const clientResponse = { code: err.responseJSON.code, feedback_code: message};

        const opt: any = sdk.getOptions();
        this.bsModelTxnDialog = this.modalService.show(TransactionDialogComponent, {
          class: "modal-md txn-modal-dialog",
          initialState: {
            data: clientResponse,
            options: sdk.getOptions(),
            localization: sdk.localization,
            onclose: () => {
              this.bsModelTxnDialog.hide();
            },
            onretry: () => {
              if (action === TransactionMode.ENROLL) {
                this.enroll();
              } else if (action === TransactionMode.VERIFY) {
                this.verify();
              } else {
                this.capture();
              }
            }
          }
        });

      });

    }));

  }


  capture() {
    this.runTransaction(TransactionMode.CAPTURE);
  }

  enroll() {
    this.runTransaction(TransactionMode.ENROLL);
  }

  verify() {
    this.runTransaction(TransactionMode.VERIFY);
  }





}
