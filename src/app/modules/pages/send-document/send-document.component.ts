import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-send-document',
  templateUrl: './send-document.component.html',
  styleUrls: ['./send-document.component.css',
  "../../../../assets/css/bootstrap.min.css",
  "../../../../assets/css/unike-face-theme.css",
]
})
export class SendDocumentComponent implements OnInit {

  public documentForm : FormGroup;
  constructor(private fbs: FormBuilder,private router:Router) { }

  ngOnInit() {
    this.getDocumentNumber();
  }

  getDocumentNumber() {
    this.documentForm = this.fbs.group({
        documentNumber: [
            '',
            Validators.compose([

                Validators.required,
            ]),
        ]
    });
    
}

getDocument(){
  console.log(this.documentForm.value);
  if(this.documentForm.value["documentNumber"] == "12345678"){
    this.router.navigate(['/recognition']);
  }else if(this.documentForm.value["documentNumber"] == ''){
    document.getElementById('documentNumber').style.display = 'block';
      setTimeout(function () {
         document.getElementById('documentNumber').style.display = 'none'; // "foo" é o id do elemento que seja manipular.
      }, 4000);
      
  }else {
    this.router.navigate(['/register']);
  }
}

}
