import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css',
  "../../../../assets/css/bootstrap.min.css",
  "../../../../assets/css/unike-face-theme.css"]
})
export class RegisterComponent implements OnInit {

  public formRegister : FormGroup;
  constructor(private fbs: FormBuilder,private router: Router) { }

  ngOnInit() {
    this.registerFaceForm();
  }

  registerFaceForm(){
    this.formRegister = this.fbs.group({
      name: [
          '',
          Validators.compose([

              Validators.required,
          ]),
      ],
      email: [
          '',
          Validators.compose([

              Validators.required,
          ]),
      ],
  });
  }

  cadastrarFace(){
    console.log(this.formRegister.value);
    if(this.formRegister.value["email"] != '' && this.formRegister.value["name"] != ''){
      
      this.router.navigate(["/recognition"]);
    }else {
      document.getElementById('registerFace').style.display = 'block';
      setTimeout(function () {
         document.getElementById('registerFace').style.display = 'none'; // "foo" é o id do elemento que seja manipular.
      }, 4000);
    } 
  }

} 
