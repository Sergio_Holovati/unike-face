import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recognition',
  templateUrl: './recognition.component.html',
  styleUrls: ['./recognition.component.css',"./css/bootstrap.min.css","./css/unike-face-theme.css"],
 
})

export class RecognitionComponent implements OnInit {

  constructor(private route:Router) { }

  ngOnInit() {
  }

  toScore(){
    this.route.navigate(["/score"]);
  }

}
