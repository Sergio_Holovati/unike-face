import { Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { Usuario } from './login.models';
import {Header} from '../../../auth/Header.model';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn:'root'
})

export class LoginService {

    header:Header;

    constructor(private http: HttpClient){}
    baseUrl = 'http://localhost:8080/unike/login';


   

    LoginUser(usuario : Usuario) : Observable<Usuario>{   
      console.log(usuario);      
        return this.http.post<Usuario>(this.baseUrl, usuario);       
        
    }


  
}