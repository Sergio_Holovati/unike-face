
import { HttpClient} from '@angular/common/http';
import { MatSnackBar } from "@angular/material/snack-bar";

import { Router} from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn:'root'
})

export class TokenService {

  

    constructor(private http: HttpClient , private router : Router,private snackBar: MatSnackBar){}
    baseUrl = 'https://unikode.unike.tech:8001/api/unikode/v1/login'


    showMessage(msg: string, isError: boolean = false): void {
        this.snackBar.open(msg, "X", {
          duration: 3000,
          horizontalPosition: "left",
          verticalPosition: "top",
          panelClass: isError ? ["msg-error"] : ["msg-success"],
        });
      }



  
}