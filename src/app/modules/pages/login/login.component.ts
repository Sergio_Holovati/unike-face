import { LoginService } from "./login-models/login.service";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: [
    "./login.component.css",
    "../../../../assets/css/bootstrap.min.css",
    "../../../../assets/css/unike-face-theme.css",
  ],
})
export class LoginComponent implements OnInit {
  public signinForm!: FormGroup;
  constructor(
    private fbs: FormBuilder,
    private router: Router,
    private login: LoginService
  ) {}

  ngOnInit(): void {
    this.signinUserForm();
  }

  signinUserForm() {
    this.signinForm = this.fbs.group({
      email: ["", Validators.compose([Validators.required])],
      password: ["", Validators.compose([Validators.required])],
    });
  }

  loginUsuario(): void {
    this.login.LoginUser(this.signinForm.value).subscribe(
      (resultado) => {
        localStorage.setItem("token", resultado["token"]);
        this.router.navigate(["/send-document"]);
      },
      (erro) => {
        if (erro.status) {
          alert("Usuario não localizado");
          localStorage.clear();
        }
      }
    );

    /* if(this.signinForm.value["email"] == "sergio@unike.tech" && this.signinForm.value["password"] == "unikode123"){
      this.router.navigate(['/send-document']); 
        }else if(this.signinForm.value["email"] == ''  || this.signinForm.value["password"] == '' ){
      document.getElementById('login').style.display = 'block';
      setTimeout(function () {
         document.getElementById('login').style.display = 'none'; // "foo" é o id do elemento que seja manipular.
      }, 4000);
      
     }else {
      document.getElementById('login').style.display = 'block';
      setTimeout(function () {
         document.getElementById('login').style.display = 'none'; // "foo" é o id do elemento que seja manipular.
      }, 4000);
      alert("senha incorreta");
     } */
  }
}
