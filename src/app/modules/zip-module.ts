import * as JsZip from "jszip";
export class ZipModule {

  async package(response: any, onlyPng: boolean = false): Promise<Blob> {
    const zip = new JsZip();
    const face = response.data;
    for (const template in face.templates) {
      if (onlyPng) {
        if (template === "PNG") {
          zip.file(`FACE.${template.toLowerCase()}`, face.templates.PNG, {base64: true});
        }
      } else {
        zip.file(`FACE.${template.toLowerCase()}`, face.templates[template], {base64: true});
      }
    }

    return zip.generateAsync({type: "blob"}).then((zipContent) => {
      return zipContent;
    });
  }

}
