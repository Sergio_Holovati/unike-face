import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppComponent } from "./app.component";
import { TitleBarComponent } from "./components/title-bar/title-bar.component";
import { TitleContextMenuComponent } from "./components/title-bar/title-context-menu/title-context-menu.component";
import { BsDropdownModule, CollapseModule  } from "ngx-bootstrap";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { SplashComponent } from "./components/title-bar/splash/splash.component";
import { ModalModule } from "ngx-bootstrap";
import { CommonModule } from "@angular/common";
import {SdkRunComponent} from "./components/sdk-run/sdk-run.component";
import {HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import { IndexComponent } from "./components/index/index.component";
import {ProgressDialogComponent} from "./components/dialogs/progress-dialog/progress-dialog.component";
import {TransactionDialogComponent} from "./components/dialogs/transaction-dialog/transaction-dialog.component";
import { RecognitionComponent } from './modules/pages/recognition/recognition.component';
import { SdkComponent } from './modules/pages/sdk/sdk.component';
import { LoginComponent } from './modules/pages/login/login.component';
import { SendDocumentComponent } from './modules/pages/send-document/send-document.component';
import { ScoreComponent } from './modules/pages/score/score.component';
import { RegisterComponent } from './modules/pages/register/register.component';
import { GlobalModalComponent } from './components/global-modal/global-modal.component';
import { TokenInterceptor } from "./modules/auth/token.interceptor";

const routes = [
  {
    path: "",
    component:LoginComponent
  },
  {
    path: "login",
    component:LoginComponent
  },
  {
    path: "app", component: IndexComponent,
  },
  {
    path:"recognition",
    component:RecognitionComponent
  },
  {
    path:"send-document",
    component:SendDocumentComponent
  },
  {
    path:"register",
    component:RegisterComponent
  },
  {
    path:"score",
    component:ScoreComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    TitleBarComponent,
    TitleContextMenuComponent,
    SplashComponent,
    SdkRunComponent,
    IndexComponent,
    ProgressDialogComponent,
    TransactionDialogComponent,
    RecognitionComponent,
    SdkComponent,
    LoginComponent,
    SendDocumentComponent,
    ScoreComponent,
    RegisterComponent,
    GlobalModalComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    HttpClientModule,
    BsDropdownModule.forRoot(),
    CollapseModule.forRoot(),
    ModalModule.forRoot(),
    RouterModule.forRoot(routes, {enableTracing: false}),
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    BrowserAnimationsModule
  ],
  providers: [
    {  provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true}
    ],
  entryComponents: [ProgressDialogComponent, TransactionDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
